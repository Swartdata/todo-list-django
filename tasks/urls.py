from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name="list"),
    path('Updating_Task/<str:pk>/', views.updateTask, name="update_task"),
    path('Deleting_Task/<str:pk>/', views.deleteTask, name="delete"),
]